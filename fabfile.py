#!/usr/bin/python
from fabric.api import *
from fabric.tasks import execute
from datetime import datetime

env.roledefs = {
    'uat': ['128.199.131.76'],
    'prod': ['128.199.132.229'],
}
env.user = 'root'
i = datetime.now()
date = i.strftime('%Y%m%d_%H%M')

def set_hosts(ip=None):
  env.hosts = ['128.199.131.76']
  if ip != "":
    env.hosts.append(ip)

def run_unit_test():
  local("python manage.py test polls")
   
def create_tag():
    local("git tag release_{0}".format(date))
    local("git push origin release_{0}".format(date))

def get_source(version=None):
    run("rm -rf {0}".format(version))
    run("git clone --branch {0} https://vankhoa101@bitbucket.org/vankhoa101/demo.git {1}".format(version,version))

def create_link(version=None):
  
    run("rm -rf demo")
    run("ln -sf {0} demo".format(version))

def restart_gunicorn():
    run("supervisorctl restart gunicorn")


def deploy_uat(path=None, version="", ip=None):
   
  if version == "":
    version = local("git -C {0} describe --tags".format(path),capture=True)
  
  with cd('/root/Devel/godemo'):
    get_source(version)
    create_link(version)
    restart_gunicorn()

@roles('prod')
def deploy_prod(path=None, version=""):
  if version == "":
    version = local("git -C {0} describe --tags".format(path),capture=True)
 
  with cd('/root/Devel/godemo'):
    get_source(version)
    create_link(version)
    restart_gunicorn()
 
@roles('prod')
def roll_back(version=""):
  with cd('/root/Devel/godemo'):
    run("rm -rf demo")
    if version == "":
      version = run("ls -lrt | tail -n 2 | head -n 1 | awk '{print $9}'")
    run("ln -sf {0} demo".format(version))
    restart_gunicorn()

  