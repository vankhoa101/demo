from django.test import TestCase
from django.core.urlresolvers import reverse
# Create your tests here.
from django.test import Client
class IndexTests(TestCase):
    def test_index_view(self):
        
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Hello World")
        
